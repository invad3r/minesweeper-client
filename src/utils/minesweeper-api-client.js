export class ApiClient {
  constructor(token) {
    this.token = token;
    this.host = process.env.REACT_APP_API_HOST;
    this.path = '/minesweeper/game';
  }

  async saveGame(data) {
    const { host, path, token } = this;
    const response = await fetch(`${host}${path}`, {
      method: 'POST',
      headers: {
         'Authorization': token,
         'Content-Type': 'application/json'
      },
      body: JSON.stringify({ data }),
    });
    if (response.status !== 200) {
      throw new Error(`server responded with a ${response.status} code`);
    }
    const game = await response.json();
    return game;
  }

  async fetchGames() {
    const { host, path, token } = this;
    const response = await fetch(`${host}${path}?accessToken=${token}`);
    if (response.status !== 200) {
      throw new Error(`server responded with a ${response.status} code`);
    }
    const { games } = await response.json();
    return games;
  }
}
