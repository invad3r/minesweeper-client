import jsonwebtoken from 'jsonwebtoken';
import { ApiClient } from './../utils/minesweeper-api-client';

export const loadState = (game) => {
    return {
        type: 'LOAD_STATE',
        game
    }
};


export const newSession = (session) => {
    return {
        type: 'NEW_SESSION',
        session
    }
};


const saveGame = async (dispatch, getState) => {
    const {
      accountState,
      timerState,
      gameSettings,
      gameState
    } = getState();


    const mineClient = new ApiClient(accountState.token);
    let game;

    try {
      game = await mineClient.saveGame({ timerState, gameSettings, gameState });
    } catch(e) {
      return; //exit dispatcher
    }

    accountState.games.push(game);
    return dispatch(loadState({ accountState }))
}


export function handleSaveGame() {
    return function(dispatch, getState) {
        saveGame(dispatch, getState)
    }
}

const loadGame = async (game, dispatch, getState) => {
    return dispatch(loadState(game))
}

export function handleLoadGame(game) {
    return function(dispatch, getState) {
        loadGame(game, dispatch, getState)
    }
}

const loadAccount = async (token, dispatch, getState) => {
    const mineClient = new ApiClient(token);
    let games = [];

    try {
      games = await mineClient.fetchGames();
    } catch(e) {
      return; //exit dispatcher
    }

    const { usr } = jsonwebtoken.decode(token);
    return dispatch(newSession({ username: usr, token, games }))
}

export function handleSession(token) {
    return function(dispatch, getState) {
        loadAccount(token, dispatch, getState)
    }
}
