import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Dropdown from 'react-bootstrap/Dropdown';


export default class Account extends Component {
    static propTypes = {
      accountState: PropTypes.shape({
          token: PropTypes.string,
          username: PropTypes.string.isRequired,
          games: PropTypes.array.isRequired
      }).isRequired,
      handleSaveGame: PropTypes.func.isRequired,
      handleLoadGame: PropTypes.func.isRequired
    };

    componentDidMount() {
      let { handleSession} = this.props;
      const searchParams = new URLSearchParams(window.location.search);
      const token = searchParams.get('accessToken');
      handleSession(token);
    }

    render() {
        const { handleSaveGame, handleLoadGame, accountState } = this.props;
        const { games } = accountState;
        return (
            <div>

            <div className="jumbotron">
              <h1 className="display-4">Welcome, <span style={{ color: 'red' }}>{ accountState.username }</span>!</h1>
              <p className="lead">
                This is an API powered react and redux implementation of a good old minesweeper game.
                This project was originally forked from
                <a href="https://github.com/gel1os/ReactMinesweeper/tree/master/src"> gel1os/ReactMinesweeper.</a>
                <br/>
                This new versions adds support for handling user accouts and for saving games.
              </p>
              <hr className="my-4"></hr>
              <p>Check both minesweeper-api and minesweeper-client repos on my <a href="https://gitlab.com/invad3r">Gitlab account</a>.</p>

              <div className="row">
                <div className="col-sm">
                  <div className="btn btn-primary btn-lg"
                       onClick={() => { handleSaveGame()}}>
                      Save Game
                  </div>
                </div>

                <div className="col-sm">
                  <Dropdown>
                    <Dropdown.Toggle variant="primary" size="lg" id="dropdown-basic">
                      Load Game
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                      {games.map(function(game){
                        const { ts, _id, data } = game;
                        return <Dropdown.Item
                        onClick={() => { handleLoadGame(data)}}
                        key={_id}>{ ts }</Dropdown.Item>;
                      })}
                    </Dropdown.Menu>
                  </Dropdown>
                </div>
              </div>
            </div>
          </div>
        );
    };

}
