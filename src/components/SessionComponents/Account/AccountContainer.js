import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { handleSaveGame, handleLoadGame, handleSession } from './../../../actions/accountActions'
import Account from './Account.js';

function mapStateToProps(state) {
    return {
        accountState: state.accountState
    }
}


function mapDispatchToProps(dispatch) {
    return {
            ...bindActionCreators({
            handleSession,
            handleSaveGame,
            handleLoadGame,
        }, dispatch)
    }
}

const AccountContainer = connect(mapStateToProps, mapDispatchToProps)(Account);

export default AccountContainer;
