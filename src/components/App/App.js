import React, { Component } from 'react';
import {Route} from 'react-router-dom';
import Minesweeper from './../MinesweeperComponents/Minesweeper';
import Session from './../SessionComponents/Session';
import { hot } from 'react-hot-loader'

class App extends Component {
    render() {
        return (
            <div>
                <Route component={Session}/>
                <Route component={Minesweeper}/>
            </div>
        )
    }
}
export default hot(module)(App);
