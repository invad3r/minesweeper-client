const defaultAccountState = {
    token: null,
    username: 'Guest',
    games: []
};

export const accountState = (state = defaultAccountState, action) => {
    switch (action.type) {
        case 'DUMP_STATE':
            return {
                ...state
            };

        case 'LOAD_STATE':
            return {
                ...state
            };
        case 'NEW_SESSION':
            return {
                ...state,
                ...action.session
            };
        default:
            return state || {
                token: null,
                username: 'Guest',
                games: []
            }

    };
};
