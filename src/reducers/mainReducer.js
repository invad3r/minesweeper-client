import {combineReducers} from 'redux';
import { gameSettings, gameState, timerState } from './minesweeperReducer.js';
import { accountState } from './accountReducer.js';

export default combineReducers({
    gameSettings,
    gameState,
    timerState,
    accountState
})
